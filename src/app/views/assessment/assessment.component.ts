import { Component, OnInit } from '@angular/core';
import { extend } from 'webdriver-js-extender';
import { BaseComponent } from 'src/app/base.component';
import { Store, Actions, ofActionSuccessful, Select } from '@ngxs/store';
import { AuthState } from 'src/app/store/states/authstate.state';
import { AuthStateInterface, Account } from 'src/app/store/classes/authstate.classes';
import { LogOut, SignIn, SignUp } from 'src/app/store/actions/authstate.actions';
import { APIService } from 'src/app/services/api.service';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-assessment',
    templateUrl: './assessment.component.html',
    styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent extends BaseComponent implements OnInit {
    
    State = State;
    state: State = State.Intro;

    selectedItemID: number;
    loggedInUsername: string = null;
    @Select(AuthState.signedInID) signedInID$: Observable<number>;

    items: any[] = [];

    //cd eCbas && json-server --watch db.json
    constructor(private apiService: APIService, private store: Store, private actions$: Actions, public snackBar: MatSnackBar) {
        super();
    }

    ngOnInit() {
        var self = this;
        this.subscriptions.push(this.actions$.pipe(ofActionSuccessful(SignIn)).subscribe((acc) => self.subscriptions.push(self.apiService.getItems().pipe(tap(items => {
            self.items = items;
        }), catchError(err => { let errMsg = 'Items not downloaded. Please start json-server in project root on port 3000. [Command: json-server --watch db.json]'; self.snackBar.open(errMsg, null, { duration: 3000 }); console.log(errMsg); return throwError(JSON.stringify(err)); } )).subscribe(() => {}))));
        this.subscriptions.push(this.actions$.pipe(ofActionSuccessful(SignUp)).subscribe((acc) => self.switch(State.SignIn)));
        this.subscriptions.push(this.actions$.pipe(ofActionSuccessful(LogOut)).subscribe(() => { self.items = []; self.selectedItemID = null; }));
        this.subscriptions.push(this.signedInID$.subscribe(id => {
            const account: Account = AuthState.getLoggedInAccountFrom(self.store);
            self.loggedInUsername = account ? account.Username : null;
            self.switch(State.Intro);
        }));
    }

    switch(state: State) {
        this.state = state;
    }

    isActive(state: State) {
        return this.state == state;
    }

    logOut() {
        var self = this;
        this.subscriptions.push(this.store.dispatch(new LogOut()).subscribe(() => {}, err => this.snackBar.open("Error on logout! "+err, null, { duration: 3000 })));
    }

}

enum State {
    Intro,
    SignIn,
    SignUp
}