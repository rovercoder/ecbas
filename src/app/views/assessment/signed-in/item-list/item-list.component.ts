import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { AssessmentComponent } from '../../assessment.component';
import { BaseComponent } from 'src/app/base.component';

@Component({
    selector: 'app-assessment-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent extends BaseComponent {

    @Output() select: EventEmitter<number> = new EventEmitter<number>();
    @Input() items: any[];
    selectedItemID: number = null;

    constructor() {
        super();
    }

    onSelect(selectedItemID: number) {
        this.selectedItemID = selectedItemID;
        this.select.emit(selectedItemID);
    }

}
