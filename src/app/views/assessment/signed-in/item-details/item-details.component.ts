import { Component, OnInit, Input } from '@angular/core';
import { AssessmentComponent } from '../../assessment.component';
import { BaseComponent } from 'src/app/base.component';

@Component({
    selector: 'app-assessment-item-details',
    templateUrl: './item-details.component.html',
    styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent extends BaseComponent {

    @Input()
    set selectedItemID(value: number) {
        this._selectedItemID = value;
        this.setSelectedItem();
    }
    get selectedItemID(): number {
        return this._selectedItemID;
    }
    @Input() items: any[];
    
    private _selectedItemID: number;
    selectedItem: any;

    constructor() {
        super();
    }

    setSelectedItem() {
        this.selectedItem = this._selectedItemID == null || this._selectedItemID == undefined
            ? null
            : this.items.filter(x => x.id == this._selectedItemID)[0];
    }

}
