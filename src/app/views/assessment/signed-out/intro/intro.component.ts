import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { BaseComponent } from 'src/app/base.component';

@Component({
    selector: 'app-assessment-intro',
    templateUrl: './intro.component.html',
    styleUrls: ['./intro.component.scss']
})
export class IntroComponent extends BaseComponent {

    constructor(public appComponent: AppComponent) {  super(); }

}
