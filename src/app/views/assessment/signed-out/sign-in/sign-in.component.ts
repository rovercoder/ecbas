import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseComponent } from 'src/app/base.component';
import { SignInRequest } from 'src/app/classes/request.class';
import { SignIn } from 'src/app/store/actions/authstate.actions';

@Component({
    selector: 'app-assessment-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent extends BaseComponent {

    @Output() cancel : EventEmitter<void> = new EventEmitter();

    form: SignInRequest = new SignInRequest();
    
    constructor(private store: Store) { super(); }

    signIn() {
        this.subscriptions.push(this.store.dispatch(new SignIn(this.form)).subscribe(() => {
            this.form = <SignInRequest>{};
        }));
    }

}
