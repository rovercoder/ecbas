import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseComponent } from 'src/app/base.component';
import { SignUpRequest } from 'src/app/classes/request.class';
import { SignUp } from 'src/app/store/actions/authstate.actions';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-assessment-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent extends BaseComponent {

    @Output() cancel : EventEmitter<void> = new EventEmitter();

    form: SignUpRequest = new SignUpRequest();
    verification: { confirmPassword: string } = { confirmPassword: '' };

    constructor(private store: Store, public snackBar: MatSnackBar) {  super(); }

    signUp() {
        this.verification.confirmPassword = '';
        let form = Object.assign({}, this.form);
        this.form.password = null;
        this.subscriptions.push(this.store.dispatch(new SignUp(form)).subscribe(() => {
            this.form = new SignUpRequest();
            this.snackBar.open("Registered successfully!", null, { duration: 3000 });
        }));
    }

}
