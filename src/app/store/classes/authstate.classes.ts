import { SignUpRequest } from "src/app/classes/request.class";

export class Account {
    ID: number;
    Name: string;
    Surname: string;
    Username: string;
    Password: string;

    constructor(id: number, signUp: SignUpRequest) {
        this.ID = id;
        this.Name = signUp.name;
        this.Surname = signUp.surname;
        this.Username = signUp.username;
        this.Password = signUp.password;
    }
}

export interface AuthStateInterface {
    accounts: Account[];
    signedInID: number;
}

export const initialState: AuthStateInterface = {
    accounts: [],
    signedInID: null
};