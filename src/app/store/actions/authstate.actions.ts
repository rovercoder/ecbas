import { SignUpRequest, SignInRequest } from 'src/app/classes/request.class';

export class SignUp {
    static readonly type = '[Auth] SignUp';
    constructor(public signUp: SignUpRequest) {}
}

export class SignIn {
    static readonly type = '[Auth] SignIn';
    constructor(public signIn: SignInRequest) {}
}

export class LogOut {
    static readonly type = '[Auth] LogOut';
    constructor() {}
}

export type AuthAction = SignUp | SignIn | LogOut;
