import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { AuthStateInterface, Account, initialState } from '../classes/authstate.classes';
import { SignIn, LogOut, SignUp } from '../actions/authstate.actions';
import { APIService } from 'src/app/services/api.service';
import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { getError } from 'src/app/classes/common.class';

@State<AuthStateInterface>({
    name: 'auth',
    defaults: initialState
})
export class AuthState {

    @Selector()
    static _instance(state: AuthStateInterface) { return state; }

    @Selector()
    static signedInID(state: AuthStateInterface) {
        return state.signedInID;
    }

    static getLoggedInAccountFrom(store: Store) {
        if (!store)
            return null;
        let authState: AuthStateInterface = store.selectSnapshot(AuthState);
        if (!authState.accounts)
            return null;
        return authState.accounts.find((acc) => acc.ID == authState.signedInID);
    }

    constructor(private store: Store, private apiService: APIService, private snackbar: MatSnackBar) { }

    @Action(SignUp)
    signUp(ctx: StateContext<AuthStateInterface>, { signUp }: SignUp) {
        const state = ctx.getState();
        return this.apiService.signUp(state, signUp).pipe(tap((account: Account) => {
            if (!state.accounts) state.accounts = [];
            state.accounts.push(account);
            ctx.setState({ ...state });
        }), catchError(e => { let error = JSON.stringify(getError(e)); this.snackbar.open(error, null, { duration: 3000 }); return throwError(error); }));
    }

    @Action(SignIn)
    signIn({ getState, patchState }: StateContext<AuthStateInterface>, { signIn }: SignIn) {
        const state = getState();
        return this.apiService.signIn(state, signIn).pipe(tap((account: Account) => {
            patchState({ signedInID: account ? account.ID : null });
        }), catchError(e => { let error = JSON.stringify(getError(e)); this.snackbar.open(error, null, { duration: 3000 }); return throwError(error); }));
    }

    @Action(LogOut)
    logOut({ getState, setState }: StateContext<AuthStateInterface>, action: LogOut) {
        const state = getState();
        return this.apiService.logOut(state).pipe(tap(() => {
            state.signedInID = null;
            setState({ ...state });
        }), catchError(e => { let error = JSON.stringify(getError(e)); this.snackbar.open(error, null, { duration: 3000 }); return throwError(error); }));
    }
    
}
