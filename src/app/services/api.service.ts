import { Injectable } from "@angular/core";
import { Store } from "@ngxs/store";
import { Observable, of, empty, throwError } from "rxjs";
import { tap } from "rxjs/operators";
import { AuthState } from "../store/states/authstate.state";
import { AuthStateInterface, Account } from "../store/classes/authstate.classes";
import { SignUpRequest, SignInRequest } from "../classes/request.class";
import { _MatTabHeaderMixinBase } from "@angular/material/tabs/typings/tab-header";
import { LogOut } from "../store/actions/authstate.actions";
import { HttpClient } from "@angular/common/http";


@Injectable({
    providedIn: 'root'
})
export class APIService {

    constructor(private store: Store, private http: HttpClient) { }

    checkSession(authState: AuthStateInterface) {

        if (!authState.signedInID)
            throwError('Logged out!');

        let account: Account = authState.accounts ? authState.accounts.find(x => x.ID == authState.signedInID) : null;

        if (account)
            return of(account);

        return this.store.dispatch(new LogOut()).pipe(tap(() => {
            return throwError('Invalid session! Logged out!');
        }));
    }

    signUp(authState: AuthStateInterface, signUp: SignUpRequest) {

        if (authState.signedInID)
            return throwError('Already signed in!');

        if (authState.accounts && authState.accounts.find(x => x.Username == signUp.username))
            return throwError('Username already registered!');
        
        let newID: number = authState.accounts && authState.accounts.length > 0 ? authState.accounts.map(x => x.ID).sort((a, b) => b - a)[0] + 1 : 1;
        return of(new Account(newID, signUp));
    }

    signIn(authState: AuthStateInterface, signIn: SignInRequest) {

        let account: Account = authState.accounts ? authState.accounts.find(x => x.Username == signIn.username && x.Password == signIn.password) : null;

        if (!account)
            return throwError('Invalid username/password combination!');
        
        return of(account);
    }

    logOut(authState: AuthStateInterface) {

        if (!authState.signedInID)
            return throwError('Already logged out!');

        return of(null);
    }

    getItems() {
        return this.http.get<Object[]>('http://localhost:3000/items');
    }

}
