export class SignInRequest {
    username: string;
    password: string;
}

export class SignUpRequest {
    name: string;
    surname: string;
    username: string;
    password: string;
}