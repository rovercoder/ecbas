import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'escapeForRegex' })
export class EscapeForRegexPipe implements PipeTransform {
    transform(value: string): string {
        if (!value)
            return '';
        return value.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }
}