import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatCardModule, MatSnackBarModule } from '@angular/material';
import { FlexModule, FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { AuthState } from './store/states/authstate.state';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { environment } from 'src/environments/environment';

import { AssessmentComponent } from './views/assessment/assessment.component';
import { IntroComponent } from './views/assessment/signed-out/intro/intro.component';
import { SignInComponent } from './views/assessment/signed-out/sign-in/sign-in.component';
import { SignUpComponent } from './views/assessment/signed-out/sign-up/sign-up.component';
import { ItemDetailsComponent } from './views/assessment/signed-in/item-details/item-details.component';
import { ItemListComponent } from './views/assessment/signed-in/item-list/item-list.component';
import { EscapeForRegexPipe } from './pipes/general.pipe';

@NgModule({
    declarations: [
        AppComponent,
        AssessmentComponent,
        IntroComponent,
        SignInComponent,
        SignUpComponent,
        ItemDetailsComponent,
        ItemListComponent,
        EscapeForRegexPipe
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatSnackBarModule,
        AppRoutingModule,
        FlexModule,
        FlexLayoutModule,
        NgxsModule.forRoot([AuthState]),
        NgxsStoragePluginModule.forRoot({ key: 'library._instance' }),
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
